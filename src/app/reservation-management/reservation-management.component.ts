import {Component, OnInit} from '@angular/core';
import {ReservationsService} from '../reservations.service';
import {Reservation} from '../Reservation';
import {Rental} from '../Rental';

@Component({
  selector: 'app-reservation-management',
  templateUrl: './reservation-management.component.html',
  styleUrls: ['./reservation-management.component.css']
})
export class ReservationManagementComponent implements OnInit {

  startDate: Date;
  endDate: Date;
  reservation: Reservation;
  reservations: Reservation[];
  checkbox = {wifi: false, gps: false};

  constructor(private reservationService: ReservationsService) {
  }

  ngOnInit() {
    this.getReservations();
  }

  getReservations(): void {
    this.reservationService.getReservations()
      .subscribe(reservations => this.reservations = reservations);
    console.log(this.reservations);
  }




  addNewReservation (id: number, idOfCustomer: number, idOfCar: number, wifi: boolean, gps: boolean ): void {
    if (this.endDate < this.startDate) {
      alert('End date is before start date. ERROR!!!');
    } else {

      this.reservationService.addReservation(
        new Reservation(
          Number(id),
          idOfCustomer,
          idOfCar,
          this.endDate,
          this.startDate,
          this.checkbox.wifi,
          this.checkbox.gps
        )
      ).subscribe(
        result => {
          console.log('is ok!!!!!!!!!!!1');
          this.getReservations();
        },
        error => {
          console.log(error, 'error!!!!!!!!!!!!!!!!');
        }
      )
      ;
    }
  }

}


