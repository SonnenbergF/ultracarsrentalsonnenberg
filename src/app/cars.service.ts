import {Injectable} from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {Car} from './Car';
import {InMemoryDataService} from './in-memory-data.service';
import {CarsListComponent} from './cars-list/cars-list.component';
import {CarsComponent} from './cars/cars.component';
import {CarsserviceComponent} from './cars-service/cars-service.component';
import {Observable} from 'rxjs/Observable';
import {catchError, map, tap} from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable()
export class CarsService {


  public carsUrl = 'api/cars';

  getCars(): Observable<Car[]> {
    return this.http.get<Car[]>(this.carsUrl);
  }

  getCar(id: number): Observable<Car> {
    const url = '${this.carsUrl}/${id}';
    return this.http.get<Car>(url);
  }

  updateCar(car: Car): Observable<any> {
    return this.http.put(this.carsUrl, car, httpOptions);
  }


  addCar (car: Car): Observable<Car> {
    return this.http.post<Car>(this.carsUrl, car, httpOptions);
  }

  deleteCar(car: Car | number): Observable<Car> {
    const id = typeof car === 'number' ? car : car.id;
    const url = `${this.carsUrl}/${id}`;
    return this.http.delete<Car>(url, httpOptions);
  }

constructor(private http: HttpClient,
) { }
}
