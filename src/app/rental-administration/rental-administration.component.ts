import {Component, Input, OnInit} from '@angular/core';
import {Car} from '../Car';
import {RentalsService} from '../rentals.service';
import {RentalListComponent} from '../rental-list/rental-list.component';
import {AuthService} from '../auth.service';
import {Rental} from '../Rental';
import {CarsComponent} from '../cars/cars.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {CarDetailsModalComponent} from '../car-details-modal/car-details-modal.component';
import {RouterModule} from '@angular/router';
import {CarsService} from '../cars.service';
import {Location} from '@angular/common';
import {Ng2OrderModule, Ng2OrderPipe} from 'ng2-order-pipe';
import {Ng2SearchPipe, Ng2SearchPipeModule} from 'ng2-search-filter';

@Component({
  selector: 'app-rental-administration',
  templateUrl: './rental-administration.component.html',
  styleUrls: ['./rental-administration.component.css'],
  providers: [RentalsService, RouterModule, BsModalRef, BsModalService, Ng2SearchPipeModule, Ng2SearchPipe,
    Ng2OrderPipe, Ng2OrderModule, CarsService, CarsComponent]
})


export class RentalAdministrationComponent implements OnInit {

  @Input() car: Car;

  startDate: Date;
  endDate: Date;
  rentals: Rental[];
  rental: Rental;
  modalRef: BsModalRef;
  key: any;
  reverse: false;
  checkbox = {wifi: false, gps: false};

  constructor(private rentalsService: RentalsService, private location: Location, private modalService: BsModalService) {
  }

  ngOnInit() {

    this.getRentals();

  }

  getRentals(): void {
    this.rentalsService.getRentals()
      .subscribe(rentals => this.rentals = rentals);
    console.log(this.rentals);
  }

  goBack(): void {
    this.location.back();
  }


  openModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  addNewRental(id: number, idOfCustomer: number, idOfCar: number, wifi: boolean, gps: boolean): void {
    if (this.endDate < this.startDate) {
      alert('End date is before start date. ERROR!!!');
    } else {
      this.rentalsService.addRental(
        new Rental(
          Number(id),
          Number(idOfCustomer),
          Number(idOfCar),
          this.endDate,
          this.startDate,
          this.checkbox.wifi,
          this.checkbox.gps
        )
      ).subscribe(
        result => {
          console.log('is ok!!!!!!!!!!!1');
          this.getRentals();
        },
        error => {
          console.log(error, 'error!!!!!!!!!!!!!!!!');
        }
      )
      ;
    }
  }

  saveRental
  (id: number,
   idOfCustomer: number,
   idOfCar: number,
   endDate: Date,
   startDate: Date,
   wifi: boolean,
   gps: boolean
  ): void {
    if (this.endDate < this.startDate) {
      alert('End date is before start date. ERROR!!!');
    } else {
      this.rentalsService.updateRental(
        new Rental(
          Number(id),
          Number(idOfCustomer),
          Number(idOfCar),
          this.endDate,
          this.startDate,
          this.checkbox.wifi,
          this.checkbox.gps
        )
      ).subscribe(
        result => {
          console.log('is ok!!!!!!!!!!!1');
          this.getRentals();
        },
        error => {
          console.log(error, 'error!!!!!!!!!!!!!!!!');
        }
      )
      ;
    }
  }

  deleteRental(id: number): void {
    this.rentalsService.deleteRental(id).subscribe(
      result => {
        console.log('is ok!!!!!!!!!!!1');
        this.getRentals();
      },
      error => {
        console.log(error, 'error!!!!!!!!!!!!!!!!');
      }
    )
    ;
  }

  sort(key): void {
    this.key = key;
  }

}

