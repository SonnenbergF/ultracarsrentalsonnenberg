import { Component, OnInit } from '@angular/core';
import {CustomersServiceService} from '../customers-service.service';
import {Customer} from '../Customer';
import {BsModalRef} from 'ngx-bootstrap';
import {Rental} from '../Rental';
import {RentalsService} from '../rentals.service';

@Component({
  selector: 'app-rental-list',
  templateUrl: './rental-list.component.html',
  styleUrls: ['./rental-list.component.css']
})
export class RentalListComponent implements OnInit {

  tempFlag = true;
  selectedRental: Rental;
  rentals: Rental[];
  modalRef: BsModalRef;


  constructor(private rentalsService: RentalsService) {
  }

  ngOnInit() {
    this.getRentals();
  }

  getRentals(): void {
    this.rentalsService.getRentals()
      .subscribe(rentals => this.rentals = rentals);
  }


}
