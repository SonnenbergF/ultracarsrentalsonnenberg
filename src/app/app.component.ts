import {Component, OnInit} from '@angular/core';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'SonnenbergsUltraCarRentals';
  isAdmin: boolean;
  isUser: boolean;

  constructor(
    private router: Router,
    private auth: AuthService
  ) {

  }
  ngOnInit() {
    this.auth.isAdmin$.subscribe( (isAdmin) => {
      this.isAdmin = isAdmin;
    });
    this.auth.isUser$.subscribe( (isUSer) => {
      this.isUser = isUSer;
    });
  }
}
