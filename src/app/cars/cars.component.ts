import {Component, OnInit, Input} from '@angular/core';
import {Car} from '../Car';
import {CarsListComponent} from '../cars-list/cars-list.component';
import {CarDetailsModalComponent} from '../car-details-modal/car-details-modal.component';


@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  @Input() car: Car;
  // selectedCar: Car;
  //
  // onSelect(car: Car): void {
  //   this.selectedCar = car;
  // }
  constructor() {
  }

  ngOnInit() {
  }


}
