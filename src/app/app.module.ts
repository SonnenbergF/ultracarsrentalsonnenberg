import { AuthService } from './auth.service';
import { FormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { CarsListComponent } from './cars-list/cars-list.component';
import { CarsComponent } from './cars/cars.component';
import { CustomerManagementComponent } from './customer-management/customer-management.component';
import { CarAdministrationComponent } from './car-administration/car-administration.component';
import { CarReservationComponent } from './car-reservation/car-reservation.component';
import { RentalAdministrationComponent } from './rental-administration/rental-administration.component';

import { CustomersListComponent } from './customers-list/customers-list.component';
import { CustomersComponent } from './customers/customers.component';
import { CarsserviceComponent } from './cars-service/cars-service.component';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './/app-routing.module';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { LoginformComponent } from './loginform/loginform.component';
import { RentalListComponent } from './rental-list/rental-list.component';
import { RentalsComponent } from './rentals/rentals.component';
import {BsModalService} from 'ngx-bootstrap';
import {BsModalRef} from 'ngx-bootstrap';
import {RentalsService} from './rentals.service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { ReservationManagementComponent } from './reservation-management/reservation-management.component';
import {ReservationsService} from './reservations.service';
import {CarDetailsModalComponent} from './car-details-modal/car-details-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    CarsListComponent,
    CarsComponent,
    CustomerManagementComponent,
    CarAdministrationComponent,
    CarReservationComponent,
    RentalAdministrationComponent,
    CustomersListComponent,
    CustomersComponent,
    CarsserviceComponent,
    CustomerDetailsComponent,
    LoginformComponent,
    RentalListComponent,
    RentalsComponent,
    ReservationManagementComponent,
    CarDetailsModalComponent
  ],
  imports: [
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    // ReactiveFormsModule,
// The HttpClientInMemoryWebApiModule module intercepts HTTP requests
// and returns simulated server responses.
// Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),

AppRoutingModule
  ],
  providers: [
    CarsListComponent,
    LoginformComponent,
    CarsComponent,
    CustomersListComponent,
    CustomersComponent,
    RentalListComponent,
    RentalsComponent,
    RentalsService,
    CarAdministrationComponent,
    AuthService,
    RentalAdministrationComponent,
    CustomerManagementComponent,
    ReservationsService,
    CarDetailsModalComponent

],
  bootstrap: [AppComponent]
})
export class AppModule { }
