import {Car} from './Car';
import {Customer} from './Customer';

export class Rental {

  id: number;
  carRented: number;
  customer: number;
  startDate: Date;
  endDate: Date;
  wifi: boolean;
  gps: boolean;


  constructor(id: number, carRented: number, customer: number, startDate: Date, endDate: Date, wifi: boolean, gps: boolean) {
    this.id = id;
    this.carRented = carRented;
    this.customer = customer;
    this.startDate = startDate;
    this.endDate = endDate;
    this.wifi = wifi;
    this.gps = gps;
  }
}
