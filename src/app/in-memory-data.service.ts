import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const cars = [
      {
        id: 1,
        mark: 'Honda',
        model: 'NSX',
        colour: 'blue',
        dateOfProduction: new Date(1999, 12, 10),
        retirementDate: new Date(2018, 11, 7),
        description: 'blabla',
        mileage: 83,
        servicePeriod: new Date(2012, 12, 16)
      }, {
        id: 2,
        mark: 'Toyota',
        model: 'Supra',
        colour: 'magenta',
        dateOfProduction: new Date(2010, 3, 17),
        retirementDate: new Date(2020, 6, 14),
        description: 'blabla',
        mileage: 842,
        servicePeriod: new Date(2015, 8, 24)
      }, {
        id: 3,
        mark: 'Mercedes-Benz',
        model: 'S Class',
        colour: 'yellow',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blablabnla',
        mileage: 32,
        servicePeriod: '10,12,2012'
      }, {
        id: 4,
        mark: 'Lexus',
        model: 'LS 500',
        colour: 'green',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blabla',
        mileage: 8332,
        servicePeriod: '10,12,2012'
      }, {
        id: 5,
        mark: 'Alpina',
        model: '5 Bs',
        colour: 'Silver',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blabla',
        mileage: 842,
        servicePeriod: '10,12,2012'
      }, {
        id: 6,
        mark: 'Ferrari',
        model: '488 AllItalia',
        colour: 'black',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blabla',
        mileage: 842,
        servicePeriod: '10,12,2012'
      }, {
        id: 7,
        mark: 'Bentley',
        model: 'Coupe',
        colour: 'pink',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blabla',
        mileage: 842,
        servicePeriod: '10,12,2012'
      }, {
        id: 8,
        mark: 'Fiat',
        model: 'Uno',
        colour: 'Yellow',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blabla',
        mileage: 842,
        servicePeriod: '10,12,2012'
      }, {
        id: 9,
        mark: 'Renault',
        model: 'Clio',
        colour: 'Orange',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blabla',
        mileage: 842,
        servicePeriod: '10,12,2012'
      }, {
        id: 10,
        mark: 'Peugeot',
        model: '508',
        colour: 'Silver',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blabla',
        mileage: 842,
        servicePeriod: '10,12,2012'
      }, {
        id: 11,
        mark: 'Nissan',
        model: 'ASX',
        colour: 'Brown',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blabla',
        mileage: 842,
        servicePeriod: '10,12,2012'
      }, {
        id: 12,
        mark: 'Citroen',
        model: 'CX',
        colour: 'Yellow',
        dateOfProduction: '10,12,2012',
        retirementDate: '10,12,2012',
        description: 'blabla',
        mileage: 842,
        servicePeriod: '10,12,2012'
      }];

    const customers = [
      {
        id: 1,
        firstName: 'Jan',
        lastName: 'Kowalski',
        email: 'kowalski@gmail.com',
        address: 'ul. Opolska 23, Opole',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 984333,
        rentedCars: 3
      },
      {
        id: 2,
        firstName: 'Robert',
        lastName: 'Nowakowski',
        email: 'nowakowski@gmail.com',
        address: 'ul. Bombelska 11, Wroclaw',
        phone: 784676324,
        birthDate: '10,12,2012',
        creditCardNumber: 984233,
        rentedCars: 4
      },

      {
        id: 3,
        firstName: 'Anna',
        lastName: 'Malinowska',
        email: 'malinowska@gmail.com',
        address: 'ul. Chlebowa 88, Krakow',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 9926634,
        rentedCars: 5
      },
      {
        id: 4,
        firstName: 'Zuzanna',
        lastName: 'Branicka',
        email: 'branicka@gmail.com',
        address: 'ul. Ziemska 33, Plock',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 882844,
        rentedCars: null
      },
      {
        id: 5,
        firstName: 'Wanda',
        lastName: 'Tatarowska',
        email: 'tatarowska@gmail.com',
        address: 'ul. Mickiewicza 33/2, Sopot',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 882344,
        rentedCars: null
      },
      {
        id: 6,
        firstName: 'Piotr',
        lastName: 'Banowski',
        email: 'banowski@gmail.com',
        address: 'ul. Prosta 77/22, Gdansk',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 886864,
        rentedCars: null
      },
      {
        id: 7,
        firstName: 'Jozef',
        lastName: 'Krotki',
        email: 'krotki@gmail.com',
        address: 'ul. Wybickiego 6/2, Elblag',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 877323,
        rentedCars: 9
      }
      , {
        id: 8,
        firstName: 'Waclaw',
        lastName: 'Niemirski',
        email: 'niemirski@gmail.com',
        address: 'ul. Pozna 10/2, Warszawa',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 151244,
        rentedCars: 10
      },
      {
        id: 9,
        firstName: 'Izabela',
        lastName: 'Pcimska',
        email: 'pcimska@gmail.com',
        address: 'ul. Mickiewicza 33/2, Sopot',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 3228454,
        rentedCars: null
      },
      {
        id: 10,
        firstName: 'Krzysztof',
        lastName: 'Wyciag',
        email: 'wyciag@gmail.com',
        address: 'ul. Platter 10/23, Sopot',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 2843674,
        rentedCars: null
      },
      {
        id: 11,
        firstName: 'Tomasz',
        lastName: 'Bredzikowskiu',
        email: 'bredzikowski@gmail.com',
        address: 'ul. Tatarska 16, Wroclaw',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 8828899,
        rentedCars: 1
      },
      {
        id: 12,
        firstName: 'Lukasz',
        lastName: 'Apokrzyk',
        email: 'apokrzyk@gmail.com',
        address: 'ul. braniewska 22/8, poznan',
        phone: 123333457,
        birthDate: '10,12,2012',
        creditCardNumber: 1271735,
        rentedCars: null
      }];

    const rentals = [
      {
        id: 1,
        carRented: 2,
        customer: 3,
        startDate: new Date(2018, 1, 10),
        endDate: new Date(2018, 3, 30),
        wifi: true,
        gps: false
      },
      {
        id: 2,
        carRented: 4,
        customer: 5,
        startDate: new Date(2017, 4, 20),
        endDate: new Date(2018, 5, 23),
        wifi: true,
        gps: false
      }

    ];

    const reservations = [
      {
        id: 1,
        idOfCar: 9,
        idOfCustomer: 10,
        reservationStartDate: new Date(2018, 4, 14),
        reservationEndDate: new Date(2018, 4, 28),
        wifi: true,
        gps: false

      },
      {
        id: 2,
        idOfCar: 4,
        idOfCustomer: 2,
        reservationStartDate: new Date(2018, 3, 14),
        reservationEndDate: new Date(2018, 4, 30),
        wifi: true,
        gps: false

      }
    ];

    return {cars, customers, rentals, reservations};
  }
}
