import { AuthService } from './../auth.service';
import {Component, OnInit} from '@angular/core';
import {Router, RouterModule} from '@angular/router';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css'],
  providers: [RouterModule]
})
export class LoginformComponent implements OnInit {


  isAdmin: boolean;

  constructor(
    private router: Router,
    private auth: AuthService
  ) {

  }

  ngOnInit() {
  }

  loginUser(event) {
    event.preventDefault();
    const login = event.target.elements[0].value;
    const password = event.target.elements[1].value;
    this.auth.login(login, password);
  }

  logout() {
    this.auth.logout();
  }

}
