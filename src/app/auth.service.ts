import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthService {

  constructor() {
  }

  private isAdminSource = new Subject<boolean>();
  private isUserSource = new Subject<boolean>();

  isAdmin$ = this.isAdminSource.asObservable();
  isUser$ = this.isUserSource.asObservable();

  login(login: string, pass: string) {
    if (login === 'admin' && pass === 'admin') {
      this.isAdminSource.next(true);
    } else {
      this.isAdminSource.next(false);
    }
    if (login === 'user' && pass === 'user') {
      this.isUserSource.next(true);
    } else {
      this.isUserSource.next(false);
    }
  }

  logout () {
    this.isAdminSource.next(false);
    this.isUserSource.next(false);
  }
}
