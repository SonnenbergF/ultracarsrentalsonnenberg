import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CarsListComponent} from './cars-list/cars-list.component';
import {CarsComponent} from './cars/cars.component';
import {CustomersListComponent} from './customers-list/customers-list.component';
import {CustomerDetailsComponent} from './customer-details/customer-details.component';
import {CarAdministrationComponent} from './car-administration/car-administration.component';
import {RentalAdministrationComponent} from './rental-administration/rental-administration.component';
import {LoginformComponent} from './loginform/loginform.component';
import {RentalListComponent} from './rental-list/rental-list.component';
import {CustomerManagementComponent} from './customer-management/customer-management.component';


const routes: Routes = [
  {path: 'cars', component: CarsListComponent},
  {path: 'car_details', component: CarsComponent},
  {path: 'customers', component: CustomerManagementComponent},
  {path: 'customer_details', component: CustomerDetailsComponent},
  {path: 'cars_management', component: CarAdministrationComponent},
  {path: 'rental_management', component: RentalAdministrationComponent},
  {path: '', component: LoginformComponent}

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule {
}




