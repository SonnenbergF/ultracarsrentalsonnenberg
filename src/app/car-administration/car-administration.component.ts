import {Component, OnInit} from '@angular/core';
import {Car} from '../Car';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {CarsService} from '../cars.service';
import {LoginformComponent} from '../loginform/loginform.component';
import {CarsComponent} from '../cars/cars.component';
import {RouterModule} from '@angular/router';
import {Location} from '@angular/common';
import {Ng2OrderModule, Ng2OrderPipe} from 'ng2-order-pipe';
import {Ng2SearchPipe, Ng2SearchPipeModule} from 'ng2-search-filter';
import {CarDetailsModalComponent} from '../car-details-modal/car-details-modal.component';


@Component({
  selector: 'app-car-administration',
  templateUrl: './car-administration.component.html',
  styleUrls: ['./car-administration.component.css'],
  providers: [CarsService, RouterModule, CarDetailsModalComponent, BsModalRef, BsModalService, CarsComponent, Ng2OrderModule, Ng2OrderPipe,
    Ng2SearchPipe, Ng2SearchPipeModule]
})
export class CarAdministrationComponent implements OnInit {

  servicePeriodDateModel: Date;
  retirementDateModel: Date;
  productionDateModel: Date;
  selectedCar: Car;
  cars: Car[];
  modalRef: BsModalRef;
  car: Car;
  key: any;
  reverse: false;


  constructor(private carsService: CarsService, private loginForm: LoginformComponent,
              private carDetailsModal: CarDetailsModalComponent, private modalService: BsModalService, private location: Location) {
  }

  ngOnInit() {

    this.getCars();
    this.carDetailsModal.modalRef = this.carDetailsModal.modalRef;
  }

  getCars(): void {
    this.carsService.getCars()
      .subscribe(
        cars => {
          this.cars = cars;
          console.log(this.cars);
        });
  }

  save(id: number,
       mark: string,
       model: string,
       mileage: number,
       description: string,
       colour: string): void {
    this.carsService.updateCar(new Car(
      Number(id),
      mark,
      model,
      colour,
      this.servicePeriodDateModel,
      this.retirementDateModel,
      description,
      mileage,
      this.productionDateModel
    ))
      .subscribe(
        result => {
          console.log('is ok!!!!!!!!!!!1');
          this.getCars();
        },
        error => {
          console.log(error, 'error!!!!!!!!!!!!!!!!');
        }
      )
    ;
  }

  goBack(): void {
    this.location.back();
  }


  openModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  addNewCar(id: number,
            mark: string,
            model: string,
            mileage: number,
            description: string,
            colour: string): void {
    this.carsService.addCar(
      new Car(
        Number(id),
        mark,
        model,
        colour,
        this.servicePeriodDateModel,
        this.retirementDateModel,
        description,
        mileage,
        this.productionDateModel
      )
    ).subscribe(
      result => {
        console.log('is ok!!!!!!!!!!!1');
        this.getCars();
      },
      error => {
        console.log(error, 'error!!!!!!!!!!!!!!!!');
      }
    )
    ;
  }

  deleteCar(id: number): void {
    this.carsService.deleteCar(id).subscribe(
      result => {
        console.log('is ok!!!!!!!!!!!1');
        this.getCars();
      },
      error => {
        console.log(error, 'error!!!!!!!!!!!!!!!!');
      }
    )
    ;
  }

  openModal2(template, car: Car) {

    this.modalRef = this.modalService.show(template);

    document.getElementById('idCar').setAttribute('value', car.id.toString());
    document.getElementById('carMark').setAttribute('value', car.mark.toString());
    document.getElementById('carModel').setAttribute('value', car.model.toString());
    document.getElementById('carColor').setAttribute('value', car.colour.toString());
    document.getElementById('carProductionDate').setAttribute('value', car.dateOfProduction.toString().substr(0, 10));
    document.getElementById('carRetirementDate').setAttribute('value', car.retirementDate.toString().substr(0, 10));
    document.getElementById('description').setAttribute('value', car.description);
    document.getElementById('carMileage').setAttribute('value', car.mileage.toString());
    document.getElementById('carServicePeriod').setAttribute('value', car.servicePeriod.toString().substr(0, 10));
  }

  sort(key): void {
    this.key = key;
  }
}
