import { Component, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {CarsComponent} from '../cars/cars.component';
import {CarsListComponent} from '../cars-list/cars-list.component';

@Component({
  selector: 'app-car-details-modal',
  templateUrl: './car-details-modal.component.html',
  styleUrls: ['./car-details-modal.component.css']
})
export class CarDetailsModalComponent {
  modalRef: BsModalRef;

  constructor(private modalService: BsModalService, private cars: CarsComponent) {}

  openModal(template) {
    this.modalRef = this.modalService.show(template);
  }
}
