import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Reservation} from './Reservation';
import {Car} from './Car';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class ReservationsService {
  private reservationsUrl = 'api/reservations';

  getReservations(): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(this.reservationsUrl);
  }

  getReservation(id: number): Observable<Reservation> {
    const url = '${this.reservtionsUrl}/${id}';
    return this.http.get<Reservation>(url);
  }

  updateReservation(reservation: Reservation): Observable<any> {
    return this.http.put(this.reservationsUrl, reservation, httpOptions);
  }


  addReservation (reservation: Reservation): Observable<Reservation> {
    return this.http.post<Reservation>(this.reservationsUrl, reservation, httpOptions);
  }

  deleteReservation(reservation: Reservation | number): Observable<Reservation> {
    const id = typeof reservation === 'number' ? reservation : reservation.id;
    const url = `${this.reservationsUrl}/${id}`;
    return this.http.delete<Reservation>(url, httpOptions);
  }

  constructor(private http: HttpClient,
  ) { }
}
