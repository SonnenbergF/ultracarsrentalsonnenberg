import {Component, Input, OnInit} from '@angular/core';
import {Car} from '../Car';
import {Customer} from '../Customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {


  @Input() customer: Customer;
  constructor() { }

  ngOnInit() {
  }

}
