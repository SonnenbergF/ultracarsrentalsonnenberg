import {Component, OnInit} from '@angular/core';
import {CustomersServiceService} from '../customers-service.service';
import {Customer} from '../Customer';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {RouterModule} from '@angular/router';
import {CustomerDetailsComponent} from '../customer-details/customer-details.component';
import {CustomersComponent} from '../customers/customers.component';
import {CarDetailsModalComponent} from '../car-details-modal/car-details-modal.component';
import {LoginformComponent} from '../loginform/loginform.component';
import {Location} from '@angular/common';
import {Car} from '../Car';
import {Ng2OrderModule, Ng2OrderPipe} from 'ng2-order-pipe';
import {Ng2SearchPipe, Ng2SearchPipeModule} from 'ng2-search-filter';

@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.css'],
  providers: [RouterModule, BsModalRef, BsModalService, CustomerDetailsComponent, CustomersComponent, CustomersServiceService,
    Ng2OrderModule,
    Ng2OrderPipe, Ng2SearchPipe, Ng2SearchPipeModule]
})
export class CustomerManagementComponent implements OnInit {

  birthDate: Date;
  customers: Customer[];
  modalRef: BsModalRef;
  customer: Customer;
  key: any;
  reverse: false;


  constructor(private customerService: CustomersServiceService, private loginForm: LoginformComponent,
              private modalService: BsModalService, private location: Location) {
  }

  ngOnInit() {

    this.getCustomers();
  }


  getCustomers(): void {
    this.customerService.getCustomers()
      .subscribe(customers => this.customers = customers);
  }

  save(): void {
    this.customerService.updateCustomer(this.customer)
      .subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }


  openModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  deleteCustomer(id: number): void {
    this.customerService.deleteCustomer(id).subscribe(
      result => {
        console.log('is ok!!!!!!!!!!!1');
        this.getCustomers();
      },
      error => {
        console.log(error, 'error!!!!!!!!!!!!!!!!');
      }
    )
    ;
  }

  addNewCustomer
  (id: number,
   firstName: string,
   lastName: string,
   address: string,
   email: string,
   phone: number,
   creditCardNumber: number,
   idOfRentedCar
  ): void {
    this.customerService.addCustomer(
      new Customer(
        Number(id),
        firstName,
        lastName,
        address,
        email,
        phone,
        this.birthDate,
        creditCardNumber,
        idOfRentedCar
      )
    ).subscribe(
      result => {
        console.log('is ok!!!!!!!!!!!1');
        this.getCustomers();
      },
      error => {
        console.log(error, 'error!!!!!!!!!!!!!!!!');
      }
    )
    ;

  }

  saveCustomer
  (id: number,
   firstName: string,
   lastName: string,
   address: string,
   email: string,
   phone: number,
   creditCardNumber: number,
   idOfRentedCar
  ): void {
    this.customerService.updateCustomer(
      new Customer(
        Number(id),
        firstName,
        lastName,
        address,
        email,
        phone,
        this.birthDate,
        creditCardNumber,
        Number(idOfRentedCar)
      )
    ).subscribe(
      result => {
        console.log('is ok!!!!!!!!!!!1');
        this.getCustomers();
      },
      error => {
        console.log(error, 'error!!!!!!!!!!!!!!!!');
      }
    )
    ;

  }

  sort(key): void {
    this.key = key;
  }

}
