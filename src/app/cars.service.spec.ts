import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {CarsService} from './cars.service';
import {TestBed} from '@angular/core/testing';
import {HttpClient, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import {Car} from './Car';
import {describe} from 'selenium-webdriver/testing';


describe('CarsService (with mocks)', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let carsService: CarsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      // Import the HttpClient mocking services
      imports: [HttpClientTestingModule],
      // Provide the service-under-test
      providers: [CarsService]
    });

    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    carsService = TestBed.get(CarsService);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  /// HeroService method tests begin ///
  describe('#getCars', () => {
    let expectedCars: Car[];

    beforeEach(() => {
      carsService = TestBed.get(CarsService);
      expectedCars = [
        {
          id: 1,
          mark: 'Honda',
          model: 'NSX',
          colour: 'blue',
          dateOfProduction: new Date(1999, 12, 10),
          retirementDate: new Date(2018, 11, 7),
          description: 'blabla',
          mileage: 83,
          servicePeriod: new Date(2012, 12, 16)
        }, {
          id: 2,
          mark: 'Toyota',
          model: 'Supra',
          colour: 'magenta',
          dateOfProduction: new Date(2010, 3, 17),
          retirementDate: new Date(2020, 6, 14),
          description: 'blabla',
          mileage: 842,
          servicePeriod: new Date(2015, 8, 24)
        }
      ] as Car[];
    });

    it('should return expected car (called once)', () => {
      carsService.getCars().subscribe(
        cars => expect(cars).toEqual(expectedCars, 'should return expected cars'),
        fail
      );

      // HeroService should have made one request to GET heroes from expected URL
      const req = httpTestingController.expectOne(carsService.carsUrl);
      expect(req.request.method).toEqual('GET');

      // Respond with the mock heroes
      req.flush(expectedCars);
    });

    it('should be OK returning no cars', () => {
      carsService.getCars().subscribe(
        cars => expect(cars.length).toEqual(0, 'should have empty cars array'),
        fail
      );

      const req = httpTestingController.expectOne(carsService.carsUrl);
      req.flush([]); // Respond with no heroes
    });


    it('should return expected cars (called multiple times)', () => {
      carsService.getCars().subscribe();
      carsService.getCars().subscribe();
      carsService.getCars().subscribe(
        cars => expect(cars).toEqual(expectedCars, 'should return expected cars'),
        fail
      );

      const requests = httpTestingController.match(carsService.carsUrl);
      expect(requests.length).toEqual(3, 'calls to getCars()');

      // Respond to each request with different mock hero results
      requests[0].flush([]);
      requests[1].flush([{
        id: 1,
        mark: 'Honda',
        model: 'NSX',
        colour: 'blue',
        dateOfProduction: new Date(1999, 12, 10),
        retirementDate: new Date(2018, 11, 7),
        description: 'blabla',
        mileage: 83,
        servicePeriod: new Date(2012, 12, 16)
      }]);
      requests[2].flush(expectedCars);
    });
  });

  describe('#updateCar', () => {
    const makeUrl = (id: number) => `${carsService.carsUrl}/?id=${id}`;

    it('should update a car and return it', () => {

      const updateCar: Car = {
        id: 1,
        mark: 'Honda',
        model: 'NSX',
        colour: 'blue',
        dateOfProduction: new Date(1999, 12, 10),
        retirementDate: new Date(2018, 11, 7),
        description: 'blabla',
        mileage: 83,
        servicePeriod: new Date(2012, 12, 16)
      };

      carsService.updateCar(updateCar).subscribe(
        data => expect(data).toEqual(updateCar, 'should return the car'),
        fail
      );

      const req = httpTestingController.expectOne(carsService.carsUrl);
      expect(req.request.method).toEqual('PUT');
      expect(req.request.body).toEqual(updateCar);

      const expectedResponse = new HttpResponse(
        {status: 200, statusText: 'OK', body: updateCar});
      req.event(expectedResponse);
    });

  });

  describe('#deleteCar', () => {

    it('should delete car', () => {

      const deleteCar: Car = {
        id: 1,
        mark: 'Honda',
        model: 'NSX',
        colour: 'blue',
        dateOfProduction: new Date(1999, 12, 10),
        retirementDate: new Date(2018, 11, 7),
        description: 'blabla',
        mileage: 83,
        servicePeriod: new Date(2012, 12, 16)
      };

      carsService.deleteCar(1).subscribe(
        data => expect(data).toEqual(deleteCar, 'should delete car'),
        fail
      );
      const req = httpTestingController.expectOne(carsService.carsUrl + '/1');
      expect(req.request.method).toEqual('DELETE');
      expect(carsService.getCar(1)).toBeUndefined;

    });

  });

  describe('#addCar', () => {
    it ('should add car', () => {

        const tempCar: Car = {
          id: 1,
          mark: 'Honda',
          model: 'NSX',
          colour: 'blue',
          dateOfProduction: new Date(1999, 12, 10),
          retirementDate: new Date(2018, 11, 7),
          description: 'blabla',
          mileage: 83,
          servicePeriod: new Date(2012, 12, 16)
        };

        carsService.addCar(tempCar).subscribe(
          car => expect(car.model).toEqual('NSX', 'model should be equal')
        );

        const req = httpTestingController.expectOne(carsService.carsUrl);
        expect(req.request.method).toEqual('POST');

        req.flush(tempCar);
      });
    });
});

