import {Component, OnInit, Input} from '@angular/core';
import {Customer} from '../Customer';
import {CustomersComponent} from '../customers/customers.component';
import {RouterModule} from '@angular/router';
import {CarsService} from '../cars.service';
import {CarDetailsModalComponent} from '../car-details-modal/car-details-modal.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {CarsComponent} from '../cars/cars.component';
import {CustomerDetailsComponent} from '../customer-details/customer-details.component';
import {Car} from '../Car';
import {CustomersServiceService} from '../customers-service.service';
import {AuthService} from '../auth.service';
import {Ng2OrderModule, Ng2OrderPipe} from 'ng2-order-pipe';
import {Ng2SearchPipe, Ng2SearchPipeModule} from 'ng2-search-filter';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css'],
  providers: [RouterModule, BsModalRef, BsModalService, CustomerDetailsComponent, CustomersComponent,
    CustomersServiceService, Ng2SearchPipeModule, Ng2SearchPipe, Ng2OrderPipe, Ng2OrderModule]
})
export class CustomersListComponent implements OnInit {


  tempFlag = true;
  selectedCustomer: Customer;
  customers: Customer[];
  modalRef: BsModalRef;
  key: any;
  reverse: false;

  constructor(private customersServiceService: CustomersServiceService) {
  }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers(): void {
    this.customersServiceService.getCustomers()
      .subscribe(customers => this.customers = customers);
  }

  sort(key): void {
    this.key = key;
  }

}
