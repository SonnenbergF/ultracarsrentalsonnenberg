import {Component, OnInit} from '@angular/core';
import {Car} from '../Car';
import {CarsService} from '../cars.service';
import {RouterModule} from '@angular/router';
import {LoginformComponent} from '../loginform/loginform.component';
import {BsModalRef} from 'ngx-bootstrap';
import {BsModalService} from 'ngx-bootstrap';
import {CarsComponent} from '../cars/cars.component';
import {RentalsService} from '../rentals.service';
import {Rental} from '../Rental';
import {Ng2OrderModule, Ng2OrderPipe} from 'ng2-order-pipe';
import {Ng2SearchPipe, Ng2SearchPipeModule} from 'ng2-search-filter';
import {ReservationsService} from '../reservations.service';
import {Reservation} from '../Reservation';
import {CarDetailsModalComponent} from '../car-details-modal/car-details-modal.component';


@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.css'],
  providers: [CarsService, RouterModule,  BsModalRef, BsModalService, CarsComponent, Ng2OrderModule, Ng2OrderPipe,
    Ng2SearchPipe, Ng2SearchPipeModule, ReservationsService, RentalsService, CarDetailsModalComponent],

})
export class CarsListComponent implements OnInit {


  selectedCar: Car;
  car: Car;
  cars: Car[];
  modalRef: BsModalRef;
  rentals: Rental[];
  key: any;
  reverse: false;
  reservation: Reservation;
  reservations: Reservation[];
  endDate: Date;
  startDate: Date;
  checkbox = {wifi: false, gps: false};


  onSelect(car: Car): void {
    this.selectedCar = car;
  }

  constructor(private carsService: CarsService, private loginForm: LoginformComponent,
              private modalService: BsModalService, private rentalsService: RentalsService,
              private reservationService: ReservationsService, private carDetailsModal: CarDetailsModalComponent) {
  }

  ngOnInit() {
    this.getCars();
    this.getRentals();
    this.getReservations();
    this.carDetailsModal.modalRef = this.carDetailsModal.modalRef;
  }


  getCars(): void {
    this.carsService.getCars()
      .subscribe(cars => this.cars = cars);
  }

  getRentals(): void {
    this.rentalsService.getRentals()
      .subscribe(rentals => this.rentals = rentals);
  }

  getReservations(): void {
    this.reservationService.getReservations()
      .subscribe(reservations => this.reservations = reservations);
  }

  getCar(idOfCar: number): void {
    this.carsService.getCar(idOfCar);
  }


  addNewReservation(id: number, idOfCustomer: number, idOfCar: number, wifi: boolean, gps: boolean): void {
    if (this.endDate < this.startDate) {
      alert('End date is before start date. ERROR!!!');
    } else {
      this.reservationService.addReservation(
        new Reservation(
          Number(id),
          Number(idOfCustomer),
          Number(idOfCar),
          this.endDate,
          this.startDate,
          this.checkbox.wifi,
          this.checkbox.gps
        )
      ).subscribe(
        result => {
          console.log('is ok!!!!!!!!!!!1');
          this.getReservations();
          alert('Reservation has been created');
        },
        error => {
          console.log(error, 'error!!!!!!!!!!!!!!!!');
        }
      )
      ;
    }
  }

  openModal(template, carId: number) {

    this.modalRef = this.modalService.show(template);
  }

  openModal2(template, car: Car) {

    this.modalRef = this.modalService.show(template);

    document.getElementById('idCar').setAttribute('value', car.id.toString());
    document.getElementById('carMark').setAttribute('value', car.mark.toString());
    document.getElementById('carModel').setAttribute('value', car.model.toString());
    document.getElementById('carColor').setAttribute('value', car.colour.toString());
    document.getElementById('carProductionDate').setAttribute('value', car.dateOfProduction.toString().substr(0, 10));
    document.getElementById('carRetirementDate').setAttribute('value', car.retirementDate.toString().substr(0, 10));
    document.getElementById('description').setAttribute('value', car.description);
    document.getElementById('carMileage').setAttribute('value', car.mileage.toString());
    document.getElementById('carServicePeriod').setAttribute('value', car.servicePeriod.toString().substr(0, 10));
  }

  sort(key): void {
    this.key = key;
  }


}
