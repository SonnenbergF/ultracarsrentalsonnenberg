export class Reservation {

  id: number;
  idOfCar: number;
  idOfCustomer: number;
  reservationStartDate: Date;
  reservationEndDate: Date;
  wifi: boolean;
  gps: boolean;

  constructor(id: number, idOfCar: number,
              idOfCustomer: number, reservationStartDate: Date, reservationEndDate: Date, wifi: boolean, gps: boolean) {
    this.id = id;
    this.idOfCar = idOfCar;
    this.idOfCustomer = idOfCustomer;
    this.reservationStartDate = reservationStartDate;
    this.reservationEndDate = reservationEndDate;
    this.wifi = wifi;
    this.gps = gps;
  }
}
