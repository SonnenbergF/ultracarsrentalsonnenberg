export class Car {
  id: number;
  mark: string;
  model: string;
  colour: string;
  dateOfProduction: Date;
  retirementDate: Date;
  description: string;
  mileage: number;
  servicePeriod: Date;


  constructor(id: number, mark: string, model: string, colour: string, dateOfProduction: Date, retirementDate: Date,
              description: string, mileage: number, servicePeriod: Date) {
    this.id = id;
    this.mark = mark;
    this.model = model;
    this.colour = colour;
    this.dateOfProduction = dateOfProduction;
    this.retirementDate = retirementDate;
    this.description = description;
    this.mileage = mileage;
    this.servicePeriod = servicePeriod;
  }
}
