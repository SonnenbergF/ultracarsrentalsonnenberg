import {LinkedList} from 'ngx-bootstrap';
import {Car} from './Car';

export class Customer {

  id: number;
  firstName: string;
  lastName: string;
  address: string;
  email: string;
  phone: number;
  birthDate: Date;
  creditCardNumber: number;
  rentedCars: number;

  constructor(id: number, firstName: string, lastName: string, address: string, email: string, phone: number, birthDate: Date,
              creditCardNumber: number, rentedCars: number) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.address = address;
    this.email = email;
    this.phone = phone;
    this.birthDate = birthDate;
    this.creditCardNumber = creditCardNumber;
    this.rentedCars = rentedCars;
  }
}
