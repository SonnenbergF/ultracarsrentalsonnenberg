import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Car} from './Car';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Rental} from './Rental';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class RentalsService {


  private rentalsUrl = 'api/rentals';

  getRentals(): Observable<Rental[]> {
    return this.http.get<Rental[]>(this.rentalsUrl);
  }


  updateRental(rental: Rental): Observable<any> {
    return this.http.put(this.rentalsUrl, rental, httpOptions);
  }


  addRental (rental: Rental): Observable<Rental> {
    return this.http.post<Rental>(this.rentalsUrl, rental, httpOptions);
  }

  deleteRental(rental: Rental | number): Observable<Rental> {
    const id = typeof rental === 'number' ? rental : rental.id;
    const url = `${this.rentalsUrl}/${id}`;
    return this.http.delete<Rental>(url, httpOptions);
  }

  getRental(id: number): Observable<Rental> {
    const url = '${this.rentalsUrl}/${id}';
    return this.http.get<Rental>(url);
  }

  constructor(private http: HttpClient,
  ) { }
}
